# CPE657 OS Group Automation Tools

## How to use

Install Ansible on your machine.

```bash
sudo apt update && sudo apt upgrade -y
sudo apt install ansible
```

Then run the following command:

```bash
ansible-playbook main.yaml
```
